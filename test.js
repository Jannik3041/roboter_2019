//
// Werte in cm. 1cm = 5px


// Timer

// Variables
var setup = false

var max_degree = 180

    var max_point = 450
    var min_point = 125 
    
    var x_mid = 250
    var y_mid = 250

    var current_point = 0 // Bekommt vom json string
    var current_distance = 0 // Bekommt vom json String 
    
    var middle = 300

    var left_distance = 175
    var right_distance = 150

    var left_degree = 90/left_distance
    var right_degree = 90/right_distance

    var alpha, gamma, x_line, y_line
    //125 - 450

function setup(){
    /*
    var i, j, k, x, y = 0;
    var servo_val = 450;
    var servo_point[325][450];
    var servo_degree[i][x][y];

    for(j = 175, k = 300; k >= 125; k--, j--){
        servo_point[j][k];
    }

    for(j = 176, k = 301; k <= 450; k++, j++){
        servo_point[j][k];
    }
    */   

}
function loop(){
    drawSystem()
    drawRobot()
    calcLine()
    drawLine() 
}
var timer = window.setInterval(loop, 100)

function drawSystem(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.beginPath()
    ctx.moveTo(250, 0)
    ctx.lineTo(250, 500)
    ctx.moveTo(0, 250)
    ctx.lineTo(500, 250)
    ctx.stroke()
}

function drawRobot(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    // x, y, size, ?, ?
    ctx.beginPath()
    ctx.arc(250, 250, 10, 0, 2 * Math.PI)
    ctx.fillStyle = "black"
    ctx.fill() 
}

function drawLine(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.beginPath()
    ctx.moveTo(250, 250)
    ctx.lineTo(x_line, y_line)
    ctx.stroke()
}

function calcLine(){
    if(current_point > 300){
        alpha = (max_point - current_point) * right_degree

    } else if (current_point < 300){
        alpha = (current_point - min_point) * left_degree
    }

    gamma = 180 - alpha - 90
    
    /*
    // Errechnen von 2 Unbekannten längen. anhand dessen können wir die koordinaten ablesen
    // https://www.arndt-bruenner.de/mathe/scripts/Dreiecksberechnung.htm
    // c = b * sin(alpha) / sin(gamma)
    x_line = current_distance * sin(alpha) / sin(gamma);
    
    // b = c * sin(90) / sin(gamma)
    y_line = current_distance * sin(90) / sin(gamma);

    
   // x_line = current_distance * Math.cos(alpha);
   // y_line = x_line * Math.tan(alpha);
   */
  
   x_line = current_distance * Math.cos(alpha * Math.PI) + 250
   y_line = current_distance * Math.sin(alpha * Math.PI) + 250
}


var ti = 0;
var tc = 0;
    
function createTestValues(){
    for (ti = 0; ti <= 450; ti++){
        tc++
    }
}
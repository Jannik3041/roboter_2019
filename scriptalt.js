'use strict'

var ip = location.host
var connection = null

function start(){
    //Connection
    if(connection != null){
        connection = null
    }

    connection = new WebSocket("ws://"+ip":81)

    //Öffnen
    connection.onopen = function(){
        console.log('Websocket opend')
    }

    //Fehler beim öffnen
    connection.onerror = function(error){
        console.log('Websocket Error ', error)
    }

    //Disconnect/Close
    connection.onclose = function(){
        console.log('Websocket closed!')
    }

    //Nachricht erhalten
    connection.onmessage = function(e)[
        if(connection.readyState == 1){
            var jsObj = JSON.parse(e.data)

            if(jsObj.action == "request"){
                switch(jsObj.topic){
                    case "Forward":
                        break
                    case "Reverse":
                        break   
                    case "Left":
                        break
                    case "Right":
                        break
                    case "Moth":
                        break
                    case "Bug":
                        break
                }
            }else{
                console.log('Server: ', e.data)
            }
        }
    ]
}

function sendCMD(value){
    connection_check()

    var msg = { }
    msg.action = "response"
    msg.topic = value

    switch(msg.topic){
        case "Forward":
            break
        case "Reverse":
            break   
        case "Left":
            break
        case "Right":
            break
        case "Moth":
            break
        case "Bug":
            break    
    }
    connection.send(JSON.stringify(msg))
}

function onBodyLoad(){
    start()
    setInterval(check, 1000)
}

function connection_check(){
    if(connection == null){
        window.alert("Keine Verbindung!")
        return
    }
}

function check(){
    if(!connection){
        start()
    }
}

// ----- MAP -----
//Entwickler: Jannik Röhrig


//Bereich
var min_point = 150 //215 Mitte=390
var max_point = 450 //560
var servo_point = 0 //???
var max_alpha = 180

//Punkt der gezeichneten Linie (mid to point)
var x_line = 0
var y_line = 0

//Distanzen
var current_distance = 500
var max_distance = 500

//Alpha Werte
var alpha = -180

//Setup
var setup = false

//Daten
var data = ""

//Funktion lopp
function loop(){
    //Abfrage für das Setup
    if(setup == false){
        screencolor()
        setup = true
    }

    //Alpha >= 0 dann clear
    if(alpha >= 0){
        var c = document.getElementById("canvas_map")
        var ctx = c.getContext("2d")
        ctx.clearRect(0, 0, c.width, c.height)
        screencolor()
    }

    //Aufrufen von Funktionen pro Linie
    drawRobot()     
    drawSystem()
    line()
}

//Timer
//var timer = window.setInterval(loop, 1)

//Funktion um den Hintergrund zu färben
function screencolor(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.rect(0, 0 ,1000, 1000)
    ctx.fillStyle = "black"
    ctx.fill() 
}

//Funktion zum zeichnen des Koordinatensystems
function drawSystem(){                                     
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    
    ctx.beginPath()
    ctx.moveTo(500, 0)
    ctx.lineTo(500, 1000)
    ctx.moveTo(0, 500)
    ctx.lineTo(1000, 500)
    ctx.strokeStyle = "coral"
    ctx.stroke()
}

//Funktionen Zeichnet einen Punkt/Roboter
function drawRobot(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    // x, y, size, ?, ?
    ctx.beginPath()
    ctx.arc(500, 500, 10, 0, 2 * Math.PI)
    ctx.fillStyle = "red"
    ctx.fill() 
}

//Funktion für die Berechnung des Punktes der Linie (mid to point)
function line(){
    if(alpha > 0 ){
        alpha = -180
    }

    alpha += ((max_point - min_point) / max_alpha)
	
    /*
    //Test
    if((alpha > -110 && alpha < -100) || (alpha > -50&& alpha < -45)){
        current_distance = 150
    }else{
        current_distance = max_distance
    }
    //
    */
	
	x_line = current_distance * Math.cos(alpha * (Math.PI / 180)) + 500
	y_line = current_distance * Math.sin(alpha * (Math.PI / 180)) + 500
	
	drawLine()
}

//Funktion zum zeichnen der Linie
function drawLine(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.beginPath()
    ctx.moveTo(500, 500)
    ctx.lineTo(x_line, y_line)
    ctx.strokeStyle = "#4c9141"
    ctx.stroke()
}

//Funktion um Daten zu empfangen
function map(data){	
	//Splittet das jsObj in Elemente
    var scanlist = data.value.split(",")
	
	current_distance = scanlist[0]
    servo_point = scanlist[1]
    
    console.log("Current distance = " + current_distance)
	console.log("Servo = " + servo_point)
	
	loop()
}
'use strict'                
var ip = location.host;
var connection = null; 


function start()
{

// Connection erhalten

	if (connection != null)
		connection = null;

	connection=new WebSocket("ws://"+ip+":81");

//---------Eröffnung---------------------------
  connection.onopen = function()
  {
    console.log('Websocket opened!');
		if ( window.location.pathname == "/connect.html" || window.location.pathname == "/" ) 
			sendText("Status");

		else 
		if ( window.location.pathname == "/datapage.html")
			sendText("Werte");

		
  };
  
//-------Fehler beim Öffnen---------------------
  connection.onerror = function(error)
  {
    console.log('WebSocket Error ', error);
  };
  
//-------Am schließen-------------------
  connection.onclose = function()
  {
    console.log('Websocket closed!');   
		check();																									//reconnect
		
  };
  
//-------Beim Erhalt einer Nachricht----------
  connection.onmessage = function(e)
  {
	if (connection.readyState === 1) 
		{   //Verbindung wurde hergestellt      
				var jsObj = JSON.parse(e.data);
				if (jsObj.action == "Response")
	  
// -----------Programminformationen----------------------------------------------
	  
		{
			switch(jsObj.topic)
			{ 
							
				case "btnWanzen" 	:								
																				
							break;
				case "btnMotten" 	:								
																				
										break;
				case "btnLfahren"	:								
																				
										break;
				case "btnLinien" 	:
																				
										break;			
				case "btnSuch"   	:
													
										break;
				case "btnScan"  	:
					getData(jsObj)
					sendText("btnScan");				
					break;
					
				case "btnServos" 	:
													
										break;
				case "btnLabyrinth" :
													
										break;
				case "btnPower"		:
																				
										break;
				case "btnLinks"		:
																				
										break;
				case "btnRechts"	:
																				
										break;
				case "btnVorne"		:																		
										break;
				case "btnHinten"	:
										break;	
				case "btnConnect"	: 
					if ( jsObj.value == "erfolg")
						{
							alert("Verbunden mit Netzwerk");
							location.close();
						} 
							else 
						{
							alert("Keine Verbindung");
							location.reload();
						}
																
						break;		

				case "Status"		: 
					if ( jsObj.value != "nicht verbunden")				
						{
							document.getElementById("btnConnect").innerHTML = "Disconnect";																	
						}

							document.getElementById("txtSsid").value = jsObj.value;
																		
														
					break;

				case "Werte"	:

										
					var werteobj =  [	"bplValue" ,"bprValue" , "Fahrstatus", 
										"streckeValue","tMin","tSek","WiiStatus","WiiValueX",
										"WiiValueY","WiiValueZ","ultStatus","ultValue"];

					var iw = 0;

					var wertelist = jsObj.value.split(",");

					for (iw = 0; iw < werteobj.length ; iw++)
					{

						if ( wertelist[iw] != null) 
							document.getElementById(werteobj[iw]).innerHTML = wertelist[iw]; 	
						else 		
							document.getElementById(werteobj[iw]).innerHTML = "n/a";			
							
						
						
					
					}
					sendText("Werte");
					break;
						
			}
			
				
		}

//------------Else nur ausgeben-------------------------------------------
				else 
				{						
					console.log('Server: ', e.data);  //Daten des Websocket ausgeben, wenn kein json Objekt mit "response"
				}
		}
	};
	

}

function sendCMD(id) 
{
  
  connection_check();

  // Construct a msg object containing the data the server needs to process the message from the client.
  var msg = { };
  
  msg.action = "Kommando";													//Anfrage
	msg.topic  = document.getElementById(id).id;			//ID Des Objekts

	switch(msg.topic)
	{ 
		
		case "btnWanzen" 	:								
															
								break;
		case "btnMotten" 	:								
															
								break;
		case "btnLfahren"	:								
															
								break;
		case "btnLinien" 	:
															
								break;			
		case "btnSuch"   	:
								
								break;
		case "btnScann"  	:
								
								break;
		case "btnServos" 	:
								
								break;
		case "btnLabyrinth" :
								
								break;
		case "btnPower"		:
															
								break;
		case "btnLinks"		:
															
								break;
		case "btnRechts"	:
															
								break;
		case "btnVorne"		:																		
								break;
		case "btnHinten"	:
								break;	
		case "btnConnect"	: 	

			if (document.getElementById(id).innerHTML == "Connect")								// Flag durch den Text des Buttons wird genutzt.
			{
				alert("Verbindung mit Netzwerk "+ document.getElementById("txtSsid").value );
				msg.value = document.getElementById("txtSsid").value + "/" + document.getElementById("txtPassword").value;
			}

			// brauchen kein else 

		break;
	
	}

	
	connection.send(JSON.stringify(msg));
  
}  

function sendText(txt) 
{
  connection_check();

  var msg = { };
  
  msg.action = "Kommando";													//Anfrage
  msg.topic  = txt;			
	
	connection.send(JSON.stringify(msg));
  
}  


function onBodyLoad()
{
  start();
  setInterval(check, 1000);
}



function connection_check()
{
	 if (connection == null)
	 {
    window.alert("Keine verbindung"); 
    return;
  }

}

function check()
{
  if(!connection) 
    start();
}
//------------------------------------------------- M A P --------------------------------------------

//Entwickler: Jannik Röhrig


//Bereich
var min_point = 150 //215 Mitte=390
var max_point = 450 //560
var servo_point = 0
var max_alpha = 180

//Punkt der gezeichneten Linie (mid to point)
var x_line = 0
var y_line = 0

//Distanzen
var current_distance = 500
var max_distance = 500

//Alpha Werte
var alpha = -180

//Setup
var setup = false

//Skalierung
var scale = 2

//Daten
var data = ""

//Funktion lopp
function loop(){
    //Abfrage für das Setup
    if(setup == false){
        screencolor()
        setup = true
    }

    //Alpha >= 0 dann clear
    if(alpha >= 0){
        var c = document.getElementById("canvas_map")
        var ctx = c.getContext("2d")
        ctx.clearRect(0, 0, c.width, c.height)
        screencolor()
    }

    //Empfangen von Daten
    //getData()

    //Aufrufen von Funktionen pro Linie
    drawRobot()     
    drawSystem()
    line()
}

//Timer
var timer = window.setInterval(loop, 1)

//Funktion um den Hintergrund zu färben
function screencolor(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.rect(0, 0 ,1000, 1000)
    ctx.fillStyle = "black"
    ctx.fill() 
}

//Funktion zum zeichnen des Koordinatensystems
function drawSystem(){                                     
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    
    ctx.beginPath()
    ctx.moveTo(500, 0)
    ctx.lineTo(500, 1000)
    ctx.moveTo(0, 500)
    ctx.lineTo(1000, 500)
    ctx.strokeStyle = "coral"
    ctx.stroke()
}

//Funktionen Zeichnet einen Punkt/Roboter
function drawRobot(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    // x, y, size, ?, ?
    ctx.beginPath()
    ctx.arc(500, 500, 10, 0, 2 * Math.PI)
    ctx.fillStyle = "red"
    ctx.fill() 
}

var change = false
var save_distance = 500

//Funktion für die Berechnung des Punktes der Linie (mid to point)
function line(){
    if(alpha > 0 ){
        alpha = -180
    }

    alpha += ((max_point - min_point) / max_alpha) / 1 //Eigentlich durch Scale!
	
    /*
    //Test
    if((alpha > -110 && alpha < -100) || (alpha > -50&& alpha < -45)){
        current_distance = 150
    }else{
        current_distance = max_distance
    }
    //
    */

	//if(change == false){
   		x_line = current_distance * Math.cos(alpha * (Math.PI / 180)) + 500
		y_line = current_distance * Math.sin(alpha * (Math.PI / 180)) + 500
	/*	save_distance = current_distance
		change = true
	}else if(change == true){
		x_line = save_distance * Math.cos(alpha * (Math.PI / 180)) + 500
		y_line = save_distance * Math.sin(alpha * (Math.PI / 180)) + 500
		change = false	
	}*/
	drawLine()
}

//Funktion zum zeichnen der Linie
function drawLine(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.beginPath()
    ctx.moveTo(500, 500)
    ctx.lineTo(x_line, y_line)
    ctx.strokeStyle = "#4c9141"
    ctx.stroke()
}

//Funktion um Daten zu empfangen
function getData(data){

    var scanlist = data.value.split(",")
					
	current_distance = scanlist[0]
    servo_point = scanlist[1]
    
    console.log("Current distance = " + current_distance)
    console.log("Servo = " + servo_point)
}
#include <Adafruit_MCP23017.h>
#include <LiquidCrystal_I2C.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#include <string.h>

#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "WiFi.h"
#include <WebSocketsServer.h>
#include <ArduinoJson.h>

#include <SPIFFS.h>

#define TOPIC param.topic


Adafruit_MCP23017 io1;
Adafruit_MCP23017 io2;
LiquidCrystal_I2C lcd(0x27, 16, 2);   // set the LCD address to 0x27 for a 16 chars and 2 line display

using namespace std;


/* ------------ ULTRASCHALLBIBLIOTHEKEN ----------------*/
/* 150 - 450 , 500 schicken

  #include <NewPing.h>

  #define TRIGGER_PIN  4  // Arduino pin tied to trigger pin on the ultrasonic sensor.
  #define ECHO_PIN     5  // Arduino pin tied to echo pin on the ultrasonic sensor.
  #define MAX_DISTANCE 400 // Maximum distance  is rated at 400-500cm.

  NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // Object
  
*/
/* -----------------------------------------------------*/


/* ----------------- Webobjekte------------------------------------- */
AsyncWebServer server(80);
WebSocketsServer webSocket = WebSocketsServer(81);


bool con = false;

int ws_num;
/* ---------------Prototypen------------------- */
void webSocketEvent(uint8_t , WStype_t, uint8_t * , size_t );
void sendResponse();

//   WLAN Funktionen
void wlan_connect(char* );      //Website
void wlan_ap();
void load_website();

void command(JsonObject);
void response(JsonObject*);

/* -------------------Struktur----------------------------------- */

/* Nachrichtenstruktur */

struct TData
{
  char topic[40];
  char value[40];
  boolean bChange = false;
};

struct TData param;

/* ----------------------Einstellung----------------- */

void setup() {


  // Setup LCD
  lcd.init();
  lcd.backlight();

  Wire.setClock(400000); /* change bus clock without initing hardware */
  Serial.begin(115200);     /* Serial initialization */

  wlan_ap();

  load_website();

}


void loop()
{

  unsigned long currentMillis = millis();
  webSocket.loop();

  if (param.bChange)                    // Überprüft ob eine aktiv ist
  {
    sendResponse();                                 //Daten über Websocket 
    param.bChange = false;                          // "Flag" für Nachrichten aus
    
  }

  

}




/* ---------------------------- WEBSOCKET UND EVENTS ----------------------------------------------------- */

void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t lenght) {
  
   ws_num = num;   // GV für Clientnummer
  
  switch (type) {

    case WStype_DISCONNECTED:
      Serial.println("Websocket disconnected!");
      con = false;
      param.bChange = false;      // Verhindert Nachrichtensendungen der Person, wird sich selbst überschreiben später.
      
      break;

    case WStype_CONNECTED:
      Serial.print("Websocket connected to ");
      Serial.println(webSocket.remoteIP(num));
      con = true;

      
  
      
      break;

    case WStype_TEXT:

      String text = String((char *) payload);
      Serial.println((String)"client " + num + (String)": " + text);

      // Process request
      DynamicJsonDocument doc(128);
      DeserializationError error = deserializeJson(doc, (char *) payload);
      if (error)
      {
        Serial.print(("deserializeJson() failed: "));
        Serial.println(error.c_str());
        return;
      }

      JsonObject root = doc.as<JsonObject>();

      command(root);

      break;
  }
}

// ------ Response --- //
void sendResponse()
{
  DynamicJsonDocument doc(128);
  JsonObject root = doc.to<JsonObject>();

  response(&root);

  char buf[512];
  size_t lenght = serializeJson(doc, buf, sizeof(buf));



  if ( root["topic"] == "btnConnect" || root["topic"] == "Status" || root["topic"] == "Werte" || root["topic"] == "btnScann" )
  {
    webSocket.sendTXT(ws_num,buf, lenght);  // Für den Fall für nur einen
  }
 else
    webSocket.broadcastTXT(buf, lenght);

  Serial.println("Nachricht nach :");
  Serial.println(ws_num);
  Serial.println(buf);                          // Nachricht die versendet wird.

}


/*




*/
/* ------------------------------------ FUNKTIONEN FÜR WEBSITE-KOMMUNIKATION ---------------------------------------*/

/*      ------------- ESP32 Kommandos und Rückantwort -------------------     */


/* ------ Kommandos über Website --------------- */
void command(JsonObject root)
{
  Serial.println("Kommandaufruf");
  
  if (root["action"] == "Kommando")
  {
    strcpy(param.topic  , root["topic"]);

    /* ------ Von der Website-Aufrufe ------- */

    /*   Funktionsaufrufe in dieser Funktion              */

    /*   nicht vorhanden aber in "Liste" : btnLinien,Servo */


    if ( !strcmp(TOPIC , "btnWanze"))
    {


    }

    else if ( !strcmp(TOPIC , "btnMotte"))
    {


    }

    else if ( !strcmp(TOPIC , "btnLfahren"))
    {


    }

    else if ( !strcmp(TOPIC , "btnLinien"))
    {


    }
    else if ( !strcmp(TOPIC , "btnSuchen"))
    {


    }

    else if ( !strcmp(TOPIC, "btnScan"))
    {

    }

    else if ( !strcmp(TOPIC, "btnServos"))
    {


    }

    else if ( !strcmp(TOPIC , "btnLabyrinth"))
    {


    }


    else if ( !strcmp(TOPIC , "btnPowerower"))
    {


    }

    else if ( !strcmp(TOPIC , "btnLinks"))
    {

    }

    else if ( !strcmp(TOPIC , "btnRechts"))
    {

    }

    else if ( !strcmp(TOPIC, "btnVorne"))
    {

    }

    else if ( !strcmp(TOPIC , "btnHinten"))
    {

    }

    else if ( !strcmp(TOPIC , "btnConnect"))
    {

      if (WiFi.status() != WL_CONNECTED)
      {
        strcpy(param.value, root["value"]);
        wlan_connect(param.value);
      }


      else if (WiFi.status() == WL_CONNECTED)
      {
        WiFi.disconnect();
        wlan_ap();
      }

    }

    else if ( !strcmp(TOPIC , "Status"))
    {

    }

    else if (!strcmp(TOPIC , "Werte"))
    {
 

    }

    else 
    {
      Serial.println("Error");
    }
    /* ----------- INFRAFORTBEFEHLE ---------------- */
    
    param.bChange = true;
  }

}

/* -------- Auswertungen zur Website ---------------  */

void response(JsonObject* root)
{
  (*root)["action"] = "Response";
  (*root)["topic"] = TOPIC;        // Gleichbleibendes Topic

  // Was übertragen werden soll wird hier festgelegt.

  // Mehrmals rausschicken -> bChange = 1 in der Funktion


  if ( !strcmp(TOPIC , "btnWanze"))
  {

  }

  else if ( !strcmp(TOPIC, "btnMotten"))
  {


  }

  else if ( !strcmp(TOPIC, "btnLfahren"))
  {


  }

  else if ( !strcmp(TOPIC , "btnLinien"))
  {


  }

  else if ( !strcmp(TOPIC , "btnSuch"))
  {


  }

  else if ( !strcmp(TOPIC , "btnScan"))
  {
    static int a = 150;
    
    if (a >= 450)
      a= 150;
      
   (*root)["value"] = (String) a +","+200;

    a+=10;
  }

  else if ( !strcmp(TOPIC, "btnConnect"))
  {
    if (WiFi.status() == WL_CONNECTED)
      (*root)["value"] = "erfolg";

    else if (WiFi.status() == WL_CONNECT_FAILED)
      (*root)["value"] = "fehl";
  }

  else if ( !strcmp(TOPIC, "Status"))
  {
    if ( WiFi.status() == 3)
      (*root)["value"] = WiFi.SSID();
    else
      (*root)["value"] = "nicht verbunden";

  }

  else if (!strcmp(TOPIC , "Werte"))
  {
     int a=10;

    (*root)["value"]= (String) a + ",";              // Syntax für values;

    
  }


}




/*




*/
/* ------------------------------------- VERBINDUNGEN  ---------------------------------------------- */

// -------WLAN-Beitritt----------
void wlan_connect(char* website)
{

  char* wssid = "";                      // W-Lan-Name
  char* wpassword = "";                  // W-Lan-Passwort

  char * *pWebsite[2];                   // Sammlung der Website-Daten

  pWebsite[0] =  &wssid;
  pWebsite[1] =  &wpassword;


  // -------Stringsplit PW, SSID -------
  const char* trenn = {"/"};
  char *ptr;                        // Pointer für Funktion

  int i = 0;

  ptr = strtok(website, trenn);

  while (ptr != NULL)
  {
    *pWebsite[i] = ptr;
    ptr = strtok(NULL, trenn);
    i++;
  }

  // ---------------------------------

  lcd.clear();
  lcd.print("W-LAN Connect :");

  // Wifi Connect with SSID and Password
  WiFi.begin(wssid, wpassword);

  int error = 0;                // Errorcounter

  while (WiFi.status() != WL_CONNECTED )
  {
    if (error >= 5)            // Bei fehlgeschlagenem Versuch ( nicht neu aufsetzen, einfach eingeschalteten AP-Modus schreiben)
    {
      Serial.println("Connection to WiFi failed");
      lcd.clear();
      lcd.print("AP-Mode");
      return;
    }
    delay(4000);
    Serial.println("Connecting to WiFi..");

    error++;
  }


  lcd.setCursor(0, 1);
  lcd.print(wssid);


  Serial.println("Connected to the WiFi network");

  Serial.print("Station IP address = ");
  Serial.println(WiFi.localIP());

}

/* ---------------------------------------------------------------------------------------------------------------  */

// ----------WLAN-AccessPoint----------

void wlan_ap()
{

  const char* pssid = "PNET_AP_E";                      // AccesPoint Name
  const char* ppw = "12345678";                         // AccesPoint Password


  /* WiFi SoftAP initialization */


  WiFi.softAP(pssid, ppw);  // Name , PW , Kanal , , Max Clients

  lcd.clear();
  lcd.print("AP Mode");
  Serial.print("Soft-AP IP address = ");
  Serial.print(WiFi.softAPIP());

}

/* ------------------------------------------------------------------------------------------------------- */

/*, ------------------------------------ Websitefunktionen --------------------------------- */
/* -----Website ladet------*/

void load_website()
{
  /*    Initialize SPIFFS     */
  if (!SPIFFS.begin(true)) {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }


  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/connect.html", "text/html");
  });

  server.on("/index.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/index.html", "text/html");
  });

  server.on("/pdcpage.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/pdcpage.html", "text/html");
  });



  server.on("/connect.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/connect.html", "text/html");
  });


  server.on("/datapage.html", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/datapage.html", "text/html");
  });

  // --------------------- STYLE -----------------------------------------------

  server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/style.css", "text/css");
  });


  server.on("/styleconnect.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/styleconnect.css", "text/css");
  });

  // Route to load style.css file
  server.on("/styledata.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/styledata.css", "text/css");
  });

  // Route to load style.css file
  server.on("/stylepdc.css", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/stylepdc.css", "text/css");
  });

  // ------------ JAVA ---------------------

  server.on("/java.js", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/java.js", "text/javascript");
  });


  // ------ IMAGES --------------------

  server.on("/images/headerBG1.jpg", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/headerBG1.jpg", "img/png");
  });

  server.on("/images/pnet.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/pnet.png", "img/png");
  });

  server.on("/images/arrowLinks.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/arrowLinks.png", "img/png");
  });

  server.on("/images/arrowRechts.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/arrowRechts.png", "img/png");
  });

  server.on("/images/arrowUnten.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/arrowUnten.png", "img/png");
  });

  server.on("/images/arrowOben.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/arrowOben.png", "img/png");
  });


  server.on("/images/robo.png", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send(SPIFFS, "/images/robo.png", "img/png");
  });


  /* HTTP server */

  server.begin();
  Serial.print("HTTP server started!");

  /* WebSocket server */

  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  Serial.print("WebSocket server started!");


}

//Entwickler: Jannik, Tobias, Elham


//Bereich
var min_point = 150 //215 Mitte=390
var max_point = 450 //560
var servo_point = 0
var max_alpha = 180

//Punkt der gezeichneten Linie (mid to point)
var x_line = 0
var y_line = 0

//Distanzen
var current_distance = 500
var max_distance = 500

//Alpha Werte
var alpha = -180

//Debugging
var test = 0
var count = 0

//Setup
var setup = false

//Skalierung
var scale = 1

//Doppelte Linie
var line = false

//Funktion lopp
function loop(){
    //Abfrage für das Setup
    if(setup == false){
        screencolor()
        setup = true
    }

    //Alpha >= 0 dann clear
    if(alpha >= 0){
        var c = document.getElementById("canvas_map")
        var ctx = c.getContext("2d")
        ctx.clearRect(0, 0, c.width, c.height)
        screencolor()
    }

    //Empfangen von Daten
    //getData()

    //Aufrufen von Funktionen pro Linie
    drawRobot()     
    drawSystem()
    calcLine()
    drawLine()  
}

//Timer
var timer = window.setInterval(loop, 1)

//Funktion um den Hintergrund zu färben
function screencolor(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.rect(0, 0 ,1000, 1000)
    ctx.fillStyle = "black"
    ctx.fill() 
}

//Funktion zum zeichnen des Koordinatensystems
function drawSystem(){                                     
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    
    ctx.beginPath()
    ctx.moveTo(500, 0)
    ctx.lineTo(500, 1000)
    ctx.moveTo(0, 500)
    ctx.lineTo(1000, 500)
    ctx.strokeStyle = "coral"
    ctx.stroke()
}

//Funktionen Zeichnet einen Punkt/Roboter
function drawRobot(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    // x, y, size, ?, ?
    ctx.beginPath()
    ctx.arc(500, 500, 10, 0, 2 * Math.PI)
    ctx.fillStyle = "red"
    ctx.fill() 
}

//Funktion für die Berechnung des Punktes der Linie (mid to point)
function calcLine(){
    if(alpha > 0 ){
        alpha = -180
    }

    alpha += ((max_point - min_point) / max_alpha) / scale
    
    
    //Test
    if((alpha > -110 && alpha < -100) || (alpha > -50&& alpha < -45)){
        current_distance = 400
    }else{
        current_distance = max_distance
    }
    //
    

    x_line = current_distance * Math.cos(alpha * (Math.PI / 180)) + 500
    y_line = current_distance * Math.sin(alpha * (Math.PI / 180)) + 500
}

//Funktion zum zeichnen der Linie
function drawLine(){
    var c = document.getElementById("canvas_map")
    var ctx = c.getContext("2d")
    ctx.beginPath()
    ctx.moveTo(500, 500)
    ctx.lineTo(x_line, y_line)
    ctx.strokeStyle = "#4c9141"
    ctx.stroke()
}

//Funktion um Daten zu empfangen
function getData(data){

    var scanlist = data.split(",")
					
	current_distance = scanlist[0]
    servo_point = scanlist[1]
    
    console.log("Current distance = " + current_distance)
    console.log("Servo = " + servo_point)
}